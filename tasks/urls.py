# coding: utf-8
from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^data_generate/$', views.view_task_generate_data),
    url(r'^data_calc/$', views.view_task_calculate_stat_data),
    url(r'^deriv/$', views.view_task_derivative_data),
]
