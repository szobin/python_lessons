import os
from django.shortcuts import render
from django.http import HttpResponse
from django.conf import settings

from math import pi
import numpy as np
import pandas as pd

# Create your views here.


def get_random_data(a_max_value=100, data_rows=100, cols="ABCD"):
    return pd.DataFrame(np.random.randint(0, a_max_value, size=(data_rows, len(cols))), columns=list(cols))


def view_task_generate_data(request):
    df = get_random_data()

    data_str = df.to_csv(sep=";")
    fn = 'random_data.csv'

    response = HttpResponse(data_str, content_type='application/force-download')
    response['Content-Disposition'] = 'attachment; filename=%s' % fn
    response['Content-Length'] = len(data_str)
    return response


def view_task_calculate_stat_data(request):
    df = get_random_data(1000, 100, "A")
    df_col_a = df["A"]

    params = list()
    params.append(["Count of data items", len(df_col_a)])
    params.append(["Maximum of data items", df_col_a.max()])
    params.append(["Minimum of data items", df_col_a.min()])
    params.append(["Sum of data items", df_col_a.sum()])
    params.append(["Average value", df_col_a.mean()])
    params.append(["Standard deviation", df_col_a.std()])

    return render(request, "tasks/tasks_stat.html", {"params": params})


def view_task_derivative_data(request):
    return render(request, "tasks/tasks_deriv.html", {})
