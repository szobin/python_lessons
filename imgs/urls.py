# coding: utf-8
from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^tasks/deriv/$', views.view_tasks_deriv_img),
]
