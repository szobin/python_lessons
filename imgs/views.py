from django.http import HttpResponse, HttpResponseNotFound
from PIL import Image
import pandas as pd
import numpy as np
from io import BytesIO

# Create your views here.


def safe_get(request, name, default=""):
    v = request.GET.get(name)
    if v is None:
        v = default
    return v


def get_image_response(img, format="png"):
    response = HttpResponse(content_type="image/{}".format(format))
    if img is not None:
        img.save(response, 'png')
    return response


def view_tasks_deriv_img(request):

    f = safe_get(request, "f")  # frequency
    if f == "":
        f = np.random.random_sample()*10 + 1
    else:
        f = float(f)

    n = safe_get(request, "n")  # discrete number
    if n == "":
        n = int(100*f)
    else:
        n = int(n)

    func = safe_get(request, "func", "SIN")

    df = pd.DataFrame(np.linspace(0.0, 1.0, num=n), columns=["TIME"])
    if func.lower() == "cos":
        df[func] = np.cos(df['TIME'] * 2 * np.pi * f)
    else:
        df[func] = np.sin(df['TIME'] * 2 * np.pi * f)
    df['DERIV'] = np.gradient(df[func], df['TIME']*np.pi)

    plot = df.plot(x="TIME", grid=True, title="Frequency = {} Hz".format(round(f, 3)))
    fig = plot.get_figure()

    fig_data = BytesIO()
    fig.savefig(fig_data, format='png')

    img = Image.open(fig_data)
    return get_image_response(img)
