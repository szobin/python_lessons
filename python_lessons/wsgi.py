"""
WSGI config for python_lessons project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/2.1/howto/deployment/wsgi/
"""

import os, sys

from django.core.wsgi import get_wsgi_application

BASE_DIR = "/home"

sys.path.append(os.path.join(BASE_DIR, 'python_lessons'))
sys.path.append(os.path.join(BASE_DIR, 'python_lessons', 'python_lessons'))

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'python_lessons.settings')

application = get_wsgi_application()
