from django.shortcuts import render

# Create your views here.


def view_main_index(request):
    return render(request, 'main/main_index.html', {})
